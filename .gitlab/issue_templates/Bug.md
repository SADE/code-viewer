# Description
Just leave a short summary what the bug is about and where you came to notice it.

## I expected the following to happen:
…

## On the contrary, I observed:
…

# How to reproduce the bug
Please describe briefly how you discovered the bug and what a developer has to do to reproduce it.

_Steps:_
  * Step 1
  * Step 2
  * ...

# Severity
How much impact does this bug have on the product or further development?
  * [ ] Minor
  * [ ] Major
  * [ ] Critical
  * [ ] Blocker

# Platform
  * [ ] Unix
  * [ ] Windows
  * [ ] MacOS

# Program version
Which version of SADE did you use?

# Related Tickets
Add related issues if applicable.

/cc @mgoebel, @hynek, @mrodzis
