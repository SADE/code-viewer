Well, it seems that it is impossible to write software without bugs. That's why
we are really happy that you are here. Thank you very much! Please take a moment
to help us include the purposed bugfix to SADE by filling out the following form.

# Related Tickets
Add all related issues and especially those to be closed. Keep in mind that every
bugfix branch needs an issue that properly describes the bug beforehand. If your
fix addresses something untracked, please open a ticket at first.

## Related
## Closes

# Summary
Apart from what is mentioned in the main ticket you are going to close with this
MR, tell us what you have done to achieve this goal.

# Changelog
* [ ] I added a statement to the CHANGELOG.

/cc @mrodzis @mgoebel
