xquery version "3.1";

module namespace code-viewer-rest="http://bdn-edition.de/ns/code-view-rest";

import module namespace code-view="http://bdn-edition.de/ns/code-view";
import module namespace rest="http://exquery.org/ns/restxq";

declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";

declare
  %rest:POST("{$data}")
  %rest:path("code-viewer/{$comments}/{$prefix}")
  %output:method("xml")
function code-viewer-rest:main($data as item(), $comments as xs:string(), $prefix as xs:string()?)
as element(xhtml:pre) {
  if ($comments != ("comments", "nocomments"))
  then error( QName("http://bdn-edition.de/ns/code-view-rest", "parameter"), "parameter 1 must be “comments” or “nocomments”" )
  else
  switch ($comments)
  case "comments" return
    code-view:main(parse-xml($data), true(), $prefix)
  case "nocomments" return
    code-view:main(parse-xml($data), false(), $prefix)
    false()
  default return
};
