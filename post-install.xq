xquery version "3.1";

import module namespace cv-test = "https://sade.textgrid.de/ns/cv-test" at "test.xq";
import module namespace test="http://exist-db.org/xquery/xqsuite" at "resource:org/exist/xquery/lib/xqsuite/xqsuite.xql";

declare variable $sysout := util:log-system-out("code-view installation done.");
declare variable $tests := test:suite(util:list-functions("https://sade.textgrid.de/ns/cv-test"));

let $parameters :=
    <output:serialization-parameters
        xmlns:output="http://www.w3.org/2010/xslt-xquery-serialization">
        <output:method value="xml"/>
        <output:indent value="yes"/>
    </output:serialization-parameters>

return
    (
        util:log-system-out(serialize($tests, $parameters)),
        file:serialize(
            <tests time="{current-dateTime()}">{ $tests }</tests>,
            system:get-exist-home() || util:system-property("file.separator") || "tests-cv-results.xml",
            ()
        )
    )
